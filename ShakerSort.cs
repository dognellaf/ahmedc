﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AhmedCsharp
{
    class ShakerSort
    {
        public int[] Numbers = new int[] { 99, 67, 25, 81, 83, 81, 84, 7, 8, 15, 79, 98, 6, 63, 64, 87, 91, 11, 78, 71, 72, 48, 57, 63, 58, 69, 5, 6, 51, 88, 100, 49, 54, 24, 29, 37, 4, 12, 43, 12, 27, 22, 9, 32, 85, 73, 18, 75, 84, 96, 46, 56, 44, 3, 18, 1, 71, 23, 6, 21, 10, 6, 70, 64, 29, 98, 100, 33, 10, 43, 44, 37, 65, 53, 68, 49, 26, 86, 24, 9, 81, 69, 65, 25, 71, 82, 25, 42, 4, 31, 63, 13, 36, 32, 77, 65, 30, 76, 97, 40 };
        public void Start()
        {
            int k = Numbers.Length - 1;
            int n = 0;
            while (n != k - 1)
            {
                for (int i = n; i < k; i++)
                {
                    if (Numbers[i] > Numbers[i + 1])
                    {
                        SwitchNumber(i, 1);
                    }
                }
                k--;
                for (int i = k; i > n; i--)
                {
                    if (Numbers[i] < Numbers[i - 1])
                    {
                        SwitchNumber(i, -1);
                    }
                }
                n++;
            }

            for (int j = 0; j < Numbers.Length; j++)
            {
                Console.Write(Numbers[j] + " ");
            }
        }

        public void SwitchNumber(int i, int k)
        {
            //сохраняем значение
            int temp = Numbers[i];

            //1 <- 2
            Numbers[i] = Numbers[i + k];

            //2 <- сохраненное
            Numbers[i + k] = temp;
        }
    }
}
