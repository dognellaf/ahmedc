﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AhmedCsharp
{
    class Lection4
    {
        void Start()
        {

            //конвертация типов

            //не явное
            //byte a = 25; //0 до 2..
            //short c = a;
            //int d = c;
            //long k = d;

            //явное

            //a = d; //ошибка

            //a = Convert.ToByte(d); //все нормально

            //string au = "safa";
            //while (!long.TryParse(au, out long s))
            //{
            //    au = Console.ReadLine();
            //}

            //<тип данных>[] название 
            //int[] Matrix1 = new int[3];
            //int[] Matrix1 = new int[] { 1, 2, 3 };

            int[,] Matrix1 = new int[,] { { 1, 2, 3 }, { 3, 2, 1 }, { 4, 5, 6 } };
            int[,] Matrix2 = new int[,] { { 7, 8, 9 }, { 10, 11, 12 }, { 13, 14, 15 } };

            int[,] OutputMatrix = new int[3, 3];

            int rows = Matrix1.GetUpperBound(0) + 1;
            int columns = Matrix1.Length / rows;


            for (int y = 0; y < rows; y++)
            {
                for (int x = 0; x < columns; x++)
                {
                    OutputMatrix[y, x] = Matrix1[y, x] + Matrix2[y, x];
                    Console.Write(OutputMatrix[y, x] + " ");
                }
                Console.WriteLine();
            }

            Console.WriteLine();

            int[] Arr = new int[] { 2, 3, 4, 5 };
            int push = 1;

            for (int x = 0; x < Arr.Length; x++)
            {
                push *= Arr[x];
            }

            Console.WriteLine(push);
        }
    }
}
