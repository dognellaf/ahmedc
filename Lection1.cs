﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AhmedCsharp
{
    class Lection1
    {
        static void Start(string[] args)
        {
            //целые числа

            byte i1; // 0 - 255
            sbyte i12; // -128 - 127

            ushort i22; // 0 - 65535
            short i2; // -32768 - 32767

            int i3; // -2 147 483 648 - 2 147 483 648
            uint i32; // 0 - 4 294 967 295

            long i4; // –9 223 372 036 854 775 808 - 9 223 372 036 854 775 807
            ulong i42; //0 до 18 446 744 073 709 551 615

            //логические данные
            bool b;

            //символы
            char ch;
            string s = Console.ReadLine();

            //дробные типы
            float f = 10.3f; //-3.4*10^-38 до 3.4*10^38
            double d; // ±5.0*10^-324 до ±1.7*10^308

            object a;
            var a2 = "";





            //ветвление
            if (s == "два")
            {
                Console.WriteLine("2");
            }
            else
            {
                if (s == "три")
                {
                    Console.WriteLine("3");
                }
            }

            int Umn = 1;

            //цикл
            for (int i = 0; i < 5; i++)
            {
                Umn *= 2;
            }

            Console.WriteLine(Umn);
            Console.WriteLine();

            while (Umn < 100)
            {
                Umn = (Umn * 4) - 40;
            }

            Console.WriteLine(Umn);
            Console.WriteLine();

            do
            {
                Umn += 50;
            }
            while (Umn < 200);

            Console.WriteLine(Umn);
            Console.WriteLine();

        }
    }
}
