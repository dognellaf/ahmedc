﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AhmedCsharp
{
    class Lection2
    {
        static void Start()
        {
            double CherCost = 0.2;
            double ArbCost = 0.1;
            double BananaCost = 0.07;
            Console.WriteLine("Что Вы хотите купить?");

            string Answer = Console.ReadLine();
            string Count;
            int Gramm;
            double Sum;
            switch (Answer)
            {
                case "Бананы":
                    Console.WriteLine($"Бананы стоят: {BananaCost * 1000}");
                    Console.WriteLine("Сколько грамм?");
                    Count = Console.ReadLine();
                    Gramm = int.Parse(Count);
                    Sum = Gramm * BananaCost;
                    Console.WriteLine($"С Вас: {Math.Round(Sum, 3)} р.");
                    break;
                case "Арбуз":
                    Console.WriteLine($"Арбуз стоит: {ArbCost * 1000}");
                    Console.WriteLine("Сколько грамм?");
                    Count = Console.ReadLine();
                    Gramm = int.Parse(Count);
                    Sum = Gramm * ArbCost;
                    Console.WriteLine($"С Вас: {Math.Round(Sum, 3)} р.");
                    break;
                case "Черешня":
                    Console.WriteLine($"Черешня стоит: {CherCost * 1000}");
                    Console.WriteLine("Сколько грамм?");
                    Count = Console.ReadLine();
                    Gramm = int.Parse(Count);
                    Sum = Gramm * CherCost;
                    Console.WriteLine($"С Вас: {Math.Round(Sum, 3)} р.");
                    break;
            }
            int a = 3;
            int b = 5;
            int c = 40;
            int d = c-- - b * a;
            Console.WriteLine($"a={a}  b={b}  c={c}  d={d}");

            a = 3;
            b = 5;
            c = 40;
            d = --c - b * a;
            Console.WriteLine($"a={a}  b={b}  c={c}  d={d}");
        }
    }
}
