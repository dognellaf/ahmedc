﻿using System;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace AhmedCsharp
{
    class Program
    {
        static void Main(string[] args)
        {
            //[модификаторы] тип значения имя ([аргументы])
            Hello();
            int i = GetOne(true);
            Console.WriteLine(i);
        }
        static void Hello() => Console.WriteLine("Hello world!");

        static int GetOne(bool IsOne)
        {
            if (IsOne == true)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

    }
}
